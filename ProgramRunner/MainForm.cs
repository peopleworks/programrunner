﻿using ProgramRunner.Class;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace ProgramRunner
{
    public partial class MainForm : Form
    {
        bool minimizedToTray;
        NotifyIcon notifyIcon;

        StreamWriter sw = null;
        string ExMsj = String.Empty;
        DateTime date = DateTime.Now;
        string directorio = String.Empty;
        string parametro = String.Empty;

        #region Form
        public MainForm()
        {
            InitializeComponent();

            try
            {
                // Crear Objeto
                var MyIni = new IniFile();
                

                // Inicializar Variable
                 MyIni = new IniFile("ProgramSettings.ini");

                var ResultadoRegistro = "";

                if (MyIni.KeyExists("Password"))
                { 
                    ResultadoRegistro = MyIni.Read("Password");
                    ResultadoRegistro = StringCipher.Encrypt(ResultadoRegistro, "p@2019");
                }
                else
                {
                    MyIni.Write("Password", StringCipher.Encrypt("", "p@2019"));
                }

                // 2019.02.22 => Problema con municipios
                //if (String.IsNullOrWhiteSpace(ResultadoRegistro) && Program.LFirstPassword == false)
                //{
                //    Program.LFirstPassword = true;
                //    PasswordForm passwordForm = new PasswordForm();
                //    passwordForm.Show();
                //}
            }
            catch (Exception ex)
            {
                CreateLogFile(ex.Message, "E");

            }

            StartThread();
        }


        private void MainForm_Load(object sender, EventArgs e)
        {
            MinimizeToTray();
        }


        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            /*
            try
            {
                PasswordForm passwordForm = new PasswordForm();
                passwordForm.ShowDialog();

                if (Program.lStayRunning)
                {
                    // Cancelar y cerrar los eventos
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                CreateLogFile(ex.Message, "E");
            }

            */

            Program.lStayRunning = false;

        }

        #endregion


        protected override void WndProc(ref Message message)
        {
            try
            {
                if (message.Msg == SingleInstance.WM_SHOWFIRSTINSTANCE)
                {
                    ShowWindow();
                }
                base.WndProc(ref message);
            }
            catch (Exception ex)
            {
                CreateLogFile(ex.Message, "E");
            }
        }
        private void BtnMinToTray_Click(object sender, EventArgs e)
        {
            // Aplicación al área del icono de notificación (bandeja del sistema).
            MinimizeToTray();

        }

        void MinimizeToTray()
        {
            try
            {
                notifyIcon = new NotifyIcon();

                notifyIcon.DoubleClick += new EventHandler(NotifyIconClick);
                notifyIcon.Icon = this.Icon;
                notifyIcon.Text = "ProgramRunner : Esperando...";
                notifyIcon.Visible = true;
                this.WindowState = FormWindowState.Minimized;
                this.Hide();
                this.ShowInTaskbar = false;
                minimizedToTray = true;
            }
            catch (Exception ex)
            {
                CreateLogFile(ex.Message, "E");
            }
        }
        public void ShowWindow()
        {
            try
            {
                if (minimizedToTray)
                {
                    notifyIcon.Visible = false;
                    this.Show();
                    this.WindowState = FormWindowState.Normal;
                    minimizedToTray = false;
                }
                else
                {
                    WinApi.ShowToFront(this.Handle);
                }
            }
            catch (Exception ex)
            {
                CreateLogFile(ex.Message, "E");
            }
        }
        void NotifyIconClick(Object sender, System.EventArgs e)
        {
            ShowWindow();
        }

        void CreateLogFile(string name, string tipomsj)
        {
            if (tipomsj == "C") // Ejecución 
            { ExMsj = "Se han ejecutado correctamente : " + name; }
            else
            { ExMsj = "Ha ocurrido un error : " + name; }

            

            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + ExMsj);

                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {
                
                CreateLogFile(ex.Message, "E");
            }
        }

        void DayDirectory(string day, string path, string parameter, string name)
        {
            try
            {
                ProcessStartInfo startInfo;
                if (!String.IsNullOrEmpty(parameter))
                {
                    startInfo = new ProcessStartInfo
                    {
                        FileName = path, Arguments = parameter
                    };
                }
                else
                {
                    startInfo = new ProcessStartInfo
                    {
                        FileName = path
                    };
                }


                switch (day)
                {
                    case "mon":
                        Process.Start(startInfo);
                        CreateLogFile(name,"C");
                        Thread.Sleep(50000);
                        break;
                    case "tue":
                        Process.Start(startInfo);
                        CreateLogFile(name, "C");
                        Thread.Sleep(50000);
                        break;
                    case "wed":
                        Process.Start(startInfo);
                        CreateLogFile(name, "C");
                        Thread.Sleep(50000);
                        break;
                    case "thu":
                        Process.Start(startInfo);
                        CreateLogFile(name, "C");
                        Thread.Sleep(50000);
                        break;
                    case "fri":
                        Process.Start(startInfo);
                        CreateLogFile(name, "C");
                        Thread.Sleep(50000);
                        break;
                }
            }
            catch (Exception ex)
            {
       
                CreateLogFile(ex.Message, "E");
            }
        }



        #region Thread

        public void StartThread()
        {
            try
            {
                Thread threadGeneralComm = new Thread(new ThreadStart(ProccessStart));

                threadGeneralComm.Start();
            }
            catch (Exception ex)
            {
                CreateLogFile(ex.Message, "E");
            }
        }

        private void ProccessStart()
        {
            try
            {
                while (Program.lStayRunning)
                {
                    // do it
                    ProccessToExecute();
                    // Wait
                    Thread.Sleep(5000);
                }
            }
            catch (Exception ex)
            {
                CreateLogFile(ex.Message, "E");
            }
        }

        #endregion

        /// <summary>
        /// Procesos que se ejecutaran
        /// </summary>
        public void ProccessToExecute()
        {
            try
            {
                CultureInfo cultureInfo = new CultureInfo("en-US");
                string day = cultureInfo.DateTimeFormat.GetDayName(DateTime.Now.DayOfWeek).Substring(0, 3);
                string[] separated;
                var PostSetting = ConfigurationManager.GetSection("appSettings") as NameValueCollection;

                foreach (var key in PostSetting.AllKeys)
                {
                    string result = PostSetting[key];
                    separated = result.Split(' ');
                    directorio = separated[0];
                    string days = separated[1];
                    string hours = separated[2];
                    string status = separated[3];

                    if (directorio.Contains("?") == true)
                    {
                        var separadoSplit = directorio.Split('?');
                        directorio = separadoSplit[0];
                        parametro = separadoSplit[1];
                    }


                    DateTime ExecutionHour;

                    string[] Horas = hours.Split(',');
                    string[] Dias = days.Split(',');
                    day = day.Replace(".", "");
                    day = (Dias.Contains(day.ToLower()) == true) ? day.ToLower() : day.ToLower();

                    foreach (var ItemHour in Horas)
                    {
                        ExecutionHour = DateTime.Parse(ItemHour.ToString());

                        if (status == "C")
                        {
                            if (date.Hour == ExecutionHour.Hour)
                            {
                                DayDirectory(day.ToLower(), directorio, parametro, separated[4]);
                            }
                        }
                        else
                        {
                            if (date.TimeOfDay == ExecutionHour.TimeOfDay)
                            {
                                if (date.Hour == ExecutionHour.Hour)
                                {
                                    DayDirectory(day.ToLower(), directorio, parametro,separated[4]);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CreateLogFile(ex.Message, "E");
            }
        }



    }
}
