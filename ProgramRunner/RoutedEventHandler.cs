﻿using System;

namespace ProgramRunner
{
    internal class RoutedEventHandler
    {
        private Action<object, EventArgs> btnCerrar_Click;

        public RoutedEventHandler(Action<object, EventArgs> btnCerrar_Click)
        {
            this.btnCerrar_Click = btnCerrar_Click;
        }
    }
}