﻿namespace ProgramRunner
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.BtnMinToTray = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnMinToTray
            // 
            this.BtnMinToTray.Location = new System.Drawing.Point(131, 115);
            this.BtnMinToTray.Name = "BtnMinToTray";
            this.BtnMinToTray.Size = new System.Drawing.Size(179, 60);
            this.BtnMinToTray.TabIndex = 0;
            this.BtnMinToTray.Text = "Minimizar";
            this.BtnMinToTray.UseVisualStyleBackColor = true;
            this.BtnMinToTray.Click += new System.EventHandler(this.BtnMinToTray_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 198);
            this.Controls.Add(this.BtnMinToTray);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Program Runner 1.0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnMinToTray;
    }
}

