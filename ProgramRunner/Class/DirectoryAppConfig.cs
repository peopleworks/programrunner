﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramRunner.Class
{
    class DirectoryAppConfig
    {
        public string Name { get; set; }
        public string Ruta { get; set; }
        public string Dia { get; set; }
        public string Hora { get; set; }
        public Int32 Segundo { get; set; }
        public string Status { get; set; }
    }
}
