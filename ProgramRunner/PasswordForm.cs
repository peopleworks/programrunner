﻿using ProgramRunner.Class;
using System;
using System.Windows.Forms;

namespace ProgramRunner
{
    public partial class PasswordForm : Form
    {
        public PasswordForm()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                // Crear Objeto
                var MyIni = new IniFile();

                // Inicializar Variable
                MyIni = new IniFile("ProgramSettings.ini");

                // Buscar Información
                string ResultadoRegistro = MyIni.Read("Password");


                if (String.IsNullOrEmpty(ResultadoRegistro) || !String.IsNullOrEmpty(ResultadoRegistro))
                {
                    if (Program.LFirstPassword == true)
                    {
                        Program.LFirstPassword = false;

                        // Escribir en el Registro
                        string encryptedstring = StringCipher.Encrypt(txtPassWord.Text, "p@2019");

                        MyIni.Write("Password", encryptedstring);
                    }
                    else
                    {
                        if ( StringCipher.Decrypt(ResultadoRegistro.Trim().TrimEnd().TrimEnd(), "p@2019") == txtPassWord.Text.Trim().TrimEnd().TrimEnd())
                        {
                            Program.lStayRunning = false;
                        }
                        else
                        {
                            MessageBox.Show("Contraseña Incorrecta");
                        }
                    }
                
                    this.Close();
                }
                else
                {
                    MessageBox.Show("No puede dejar la contraseña en blanco");
                }
            }
            catch (Exception )
            {
                //CreateLogFile(ex.Message, "E");
                //throw;
            }

        }
    }
}
